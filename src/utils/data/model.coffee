cloudcode.utils.data.model =

  DEFAULT_VALUES:
    "string": ""
    "option": "default"
    "map": {}
    "array": []


  getDefaultPropertyValue: (propSpec) ->
    defaultValue = cloudcode.utils.data.model.DEFAULT_VALUES[propSpec.type]
    if defaultValue is "default"
      defaultValue = propSpec.default
    defaultValue


  create: (spec, all) ->
    model = {}
    for propName, propSpec of spec.properties
      if all or propSpec.required
        model[propName] = cloudcode.utils.data.model.getDefaultPropertyValue propSpec
    model


  createWith: (spec, props) ->
    {model} = cloudcode.utils.data
    data = model.create spec
    model.updateProperties data, spec, props


  updateProperties: (model, spec, props) ->
    for propName in props
      unless model[propName]
        model[propName] =
          cloudcode.utils.data.model.getDefaultPropertyValue spec.properties[propName]
    model


  getOptions: (spec, propName) ->
    propSpec = spec.properties[propName]
    isOption = propSpec?.type is "option"
    return isOption and propSpec.options

