cloudcode.editor.dataClient =


  getNamespaces: ->
    new RSVP.Promise (resolve, reject) ->
      setTimeout ->
          resolve Object.keys cloudcode.testData
          return
        , 1000


  getNodeData: (path) ->
    data = cloudcode.testData

    new RSVP.Promise (resolve, reject) ->
      if data[path]
        setTimeout ->
            resolve data[path]
            return
          , 1000
      else
        reject "no data found for this path"
