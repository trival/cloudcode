module.exports = (config) ->
  config.set

    # Karma configuration

    # base path, that will be used to resolve files and exclude
    basePath : ''

    frameworks: ['mocha']

    # preprocessors are run before testing
    preprocessors :
      '**/*.coffee': 'coffee'

    # list of files / patterns to load in the browser
    files : [
      'node_modules/chai/chai.js'
      'node_modules/sinon/pkg/sinon.js'
      # 'build/dist/libs.js'
      'build/dist/app.js'
      'test/browser-setup.coffee'
      'test/browser/**/*.coffee'
      'test/shared/**/*.coffee'
    ]

    # list of files to exclude
    # exclude : [''],

    # test results reporter to use
    # possible values: dots || progress || growl
    reporters : ['progress']

    # web server port
    port : 9999

    # cli runner port
    runnerPort : 9100

    # enable / disable colors in the output (reporters and logs)
    colors : true

    # level of logging
    # possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel : config.LOG_INFO

    # enable / disable watching file and executing tests whenever any file changes
    autoWatch : false

    # Start these browsers, currently available:
    # - Chrome
    # - ChromeCanary
    # - Firefox
    # - Opera
    # - Safari (only Mac)
    # - PhantomJS
    # - IE (only Windows)
    browsers : ['Chrome']

    # If browser does not capture in given timeout [ms], kill it
    captureTimeout : 5000

    # Continuous Integration mode
    # if true, it capture browsers, run tests and exit
    singleRun : true
