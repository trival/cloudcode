cloudcode.editor.codeItem =

  create: (data) ->
    {codeItem, ui: {createEditor}} = cloudcode.editor

    item =
      data: data
      show:
        source: true
        test: !!data.test
        docs: !!data.docs
        onUpdate: !!data.onUpdate
        deps: !!data.dependencies
      editors: {}

    codeItem.updateEditors item

    item


  updateEditors: (item) ->
    {show, editors, data} = item
    for editor in ["source", "test", "docs", "onUpdate"]
      doShow = show[editor]
      if doShow and not editors[editor]
        editors[editor] = cloudcode.editor.ui.createEditor value: data[editor]
    return


  open: (path) ->
    {codeItem, state: {loadedItems, activePaths}} = cloudcode.editor

    for activePath in activePaths
      return if path is activePath

    if loadedItems[path]
      codeItem.activate path
    else
      codeItem.load path
        .then ->
          codeItem.activate path
          return
    return


  load: (path) ->
    {codeItem, dataClient, state} = cloudcode.editor

    dataClient.getNodeData path
      .then codeItem.create
      .then (item) ->
        state.loadedItems[path] = item
        item
      .catch (e) -> console.error e; return


  activate: (path) ->
    {renderer, state} = cloudcode.editor
    state.activePaths.push path
    renderer.renderEditor()
    return


  close: (path) ->
    {renderer, state:{activePaths}} = cloudcode.editor

    for activePath, i in activePaths
      if activePath is path
        activePaths.splice i, 1
        renderer.renderEditor()
        return
    return


  save: (item) ->
    for key, editor of item.editors
      item.data[key] = editor.instance.getValue()
    return


  remove: (path) ->
    {state, codeItem} = cloudcode.editor

    if state.loadedItems[path]
      codeItem.close path
      delete state.loadedItems[path]
    return


  copyAs: (item) ->

  rename: (item) ->
