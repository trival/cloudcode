cloudcode.models.codeUnit =

  properties:

    path:
      type: "string"
      required: true
      validations: ["notEmptyString"]
      doc: "the namespace path of this code unit"

    type:
      type: "option"
      options: ["code", "json", "string"]
      default: "code"
      required: true
      validations: ["options"]
      doc: "type of the source"

    source:
      type: "string"
      required: true
      doc: "the editable code"

    lang:
      type: "option"
      options: ["javascript", "coffeescript", "livescript", "json", "glsl", "text"]
      default: "javascript"
      doc: "language of the code, hint for editor highlighting and compiler"

    dependencies:
      type: "map"
      valueType: "string"
      doc: "key: the required path, value: alias name inside of the code unit"

    compiled:
      type: "string"
      doc: "compiled code, not editable"

    docs:
      type: "string"
      doc: "the docstring, prepended to source"

    test:
      type: "string"
      doc: "unit test"

    onUpdate:
      type: "string"
      doc: "code to be executed after injection"
