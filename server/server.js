var cloudcode;

cloudcode = {};

cloudcode.libs = {};

cloudcode.server = {};

cloudcode.editor = {};

cloudcode.utils = {};

cloudcode.utils.data = {};

cloudcode.models = {};

cloudcode.libs.express = require('express');

cloudcode.libs.bacon = require('bacon');

cloudcode.server.app = (function() {
  var app, config, express;
  config = require("./config");
  express = cloudcode.libs.express;
  app = express();
  app.configure(function() {
    app.use(express["static"](config.CLIENT_DIR));
    app.use(express.errorHandler({
      dumpExceptions: true,
      showStack: true
    }));
  });
  return app;
})();

cloudcode.server.initRoutes = function() {
  var app;
  app = cloudcode.server.app;
};

cloudcode.server.start = function() {
  var config;
  config = require("./config");
  cloudcode.server.initRoutes();
  cloudcode.server.app.listen(config.SERVER_PORT);
  console.log("cloudcode server started on port " + config.SERVER_PORT);
};

cloudcode.server.start();
