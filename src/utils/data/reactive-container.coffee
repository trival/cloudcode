cloudcode.utils.data.reactiveContainer = (initialData) ->

  {bacon} = cloudcode.libs

  data = initialData
  updateStream = new bacon.Bus
  dataStream = new bacon.Bus

  updateStream.onValue (f) ->
    data = f data
    dataStream.push data
    return

  data: -> data
  update: (f) -> updateStream.push f; return
  dataStream: dataStream.toEventStream()
  updateStream: updateStream

