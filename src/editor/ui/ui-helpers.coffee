cloudcode.editor.ui =

  createEditor: (options = {}) ->
    {extend} = cloudcode.libs.lodash
    editorNode = null

    try
      editor = CodeMirror ((elt) -> editorNode = elt; return),
        extend {}, cloudcode.editor.constants.CM_OPTIONS_DEFAULT, options
    catch e
      console.error 'createEditor error: ', e

    instance: editor
    domNode: editorNode
