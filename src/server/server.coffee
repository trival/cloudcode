cloudcode.server.app = do ->
  config = require "./config"
  express = cloudcode.libs.express

  app = express()
  app.configure ->
    app.use express.static config.CLIENT_DIR
    app.use express.errorHandler
      dumpExceptions: true
      showStack: true
    return
  app


cloudcode.server.initRoutes = ->
  app = cloudcode.server.app
  return


cloudcode.server.start = ->
  config = require "./config"
  cloudcode.server.initRoutes()
  cloudcode.server.app.listen config.SERVER_PORT
  console.log "cloudcode server started on port " + config.SERVER_PORT
  return


cloudcode.server.start()
