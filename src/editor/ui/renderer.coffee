cloudcode.editor.renderer =


  treeNodeListComponent: (nodes) ->
    React.DOM.ul null,
      for _, node of nodes
        cloudcode.editor.renderer.TreeNodeComponent node


  TreeNodeComponent: React.createClass
    getInitialState: ->
      expanded: false

    render: ->
      {renderer, codeItem, state} = cloudcode.editor
      {li, h4} = React.DOM
      onClick = null
      className = ""

      if @props.children
        className = 'tree-branch'
        className += ' expanded' if @state.expanded
        onClick = =>
          @setState expanded: not @state.expanded; return
      else
        className = 'tree-leaf'
        onClick = => codeItem.open @props.path; return

      content = [
        h4
          onClick: onClick
          className: className
        , @props.label
      ]

      if @state.expanded
        content.push renderer.treeNodeListComponent @props.children

      li null, content


  codeUnitComponent: (item) ->
    {codeItem, renderer, namespaces} = cloudcode.editor
    {div, li, form, h4, a, button} = React.DOM
    path = item.data.path

    li null, [
      (h4 null, path)
      (a
        className: 'btn btn-close'
        onClick: -> codeItem.close path; return
      , 'x')
      (form null,
        renderer.getUnitEditors item
          .concat [
            (div className: 'btn-group', [
              (button
                className: 'btn'
                type: "button"
                onClick: -> namespaces.remove path; return
              , 'Delete')
              (button
                className: 'btn'
                type: "button"
                onClick: -> codeItem.copyAs item; return
              , 'CopyAs')
              (button
                className: 'btn'
                type: "button"
                onClick: -> codeItem.rename item; return
              , 'Rename')
              (button
                className: 'btn'
                type: "button"
                onClick: -> codeItem.save item; return
              , 'Save')
            ])
          ]
      )
    ]


  getUnitOptions: (item) ->
    {renderer, codeItem} = cloudcode.editor
    {div, h5} = React.DOM


  getUnitEditors: (item) ->
    {renderer, codeItem} = cloudcode.editor
    {div, h5} = React.DOM

    for editorKey in ['source', 'docs', 'test', 'onUpdate']
      do (editorKey) ->
        isVisible = item.show[editorKey]
        content = [
          (h5
            className: if isVisible then "open" else "close"
            onClick: ->
              item.show[editorKey] = not isVisible
              codeItem.updateEditors item
              renderer.renderEditor()
          , editorKey)
        ]
        if isVisible
          content.push renderer.EditorComponent
            editor: item.editors[editorKey]
            className: "editor-#{editorKey}-#{item.data.path.replace '.', '-'}"

        div null, content


  EditorComponent: React.createClass
    appendEditor: ->
      node = @getDOMNode()
      node.innerHTML = ""
      node.appendChild @props.editor.domNode
      @props.editor.instance.scrollIntoView null
      return
    componentDidUpdate: ->
      @appendEditor()
      return
    componentDidMount: ->
      @appendEditor()
      return
    render: ->
      React.DOM.div className: @props.className


  renderTree: () ->
    {renderer, state} = cloudcode.editor
    React.renderComponent (renderer.treeNodeListComponent state.nsTree),
      document.getElementById 'source-tree-view'
    return


  renderEditor: () ->
    {renderer, state} = cloudcode.editor
    React.renderComponent (React.DOM.ul null,
      state.activePaths.map (path) ->
        renderer.codeUnitComponent state.loadedItems[path]
    ), document.getElementById 'editor-view'
    return
