module.exports = (grunt) ->

  # load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach grunt.loadNpmTasks

  #concatenated Modules
  modulesServer = grunt.file.readJSON 'modules_server.json'
  modulesEditor = grunt.file.readJSON 'modules_editor.json'

  grunt.initConfig

    buildTempDir: "build/tmp/"
    modules: "<%= buildTempDir %>src/"
    buildDistDir: "build/dist/"
    bowerLibs: "bower_modules/"

    watch:
      options:
        livereload: true

      coffee:
        files: ['src/**/*.coffee', 'src/**/*.js']
        tasks: ['build', 'distJs']

      less:
        files: ['src/style/{,*/}*.less']
        tasks: ['less', 'concat:css' ]

      html:
        files: ['public/**/*.html']
        tasks: []


    clean:
      dist: [
        'public/script/**/*'
        'public/style/*'
        'public/font/'
        'server/server.js'
        '<%= buildTempDir %>'
        '<%= buildDistDir %>**/*'
      ]


    less:
      editor:
        src: 'src/style/main.less'
        dest: '<%= buildDistDir %>style/main.css'


    coffee:
      app:
        options:
          bare: true
        files: [
          expand: true
          src: 'src/**/*.coffee'
          dest: '<%= buildTempDir %>'
          ext: '.js'
        ]


    copy:
      js:
        expand: true
        cwd: 'src/'
        src: '**/*.js'
        filter: 'isFile'
        dest: '<%= buildTempDir %>src/'

      dist:
        files:
          'public/script/editor.js': '<%= buildDistDir %>editor.js'
          'public/script/libs.js': '<%= buildDistDir %>libs.js'
          'server/server.js': '<%= buildDistDir %>server.js'

      build:
        files:
          '<%= buildDistDir %>editor.js': '<%= buildDistDir %>editor.build.js'
          '<%= buildDistDir %>server.js': '<%= buildDistDir %>server.build.js'

      assetFonts:
        expand: true
        cwd: 'bower_modules/font-awesome/font/'
        src: '*'
        flatten: true
        filter: 'isFile'
        dest: 'public/font/'


    concat:
      css:
        src: [
          '<%= bowerLibs %>codemirror/lib/codemirror.css'
          '<%= bowerLibs %>codemirror/theme/monokai.css'
          '<%= buildDistDir %>style/*'
        ]
        dest: 'public/style/style.css'

      js:
        files:
          '<%= buildDistDir %>editor.build.js': modulesEditor
          '<%= buildDistDir %>server.build.js': modulesServer

      libs:
        src: [
          '<%= bowerLibs %>jquery/dist/jquery.min.js'
          '<%= bowerLibs %>lodash/dist/lodash.min.js'
          '<%= bowerLibs %>bacon/dist/Bacon.min.js'
          '<%= bowerLibs %>react/react.js'
          '<%= bowerLibs %>rsvp/rsvp.min.js'
          '<%= bowerLibs %>codemirror/lib/codemirror.js'
          '<%= bowerLibs %>codemirror/mode/javascript/javascript.js'
          '<%= bowerLibs %>codemirror/mode/coffeescript/coffeescript.js'
          '<%= bowerLibs %>codemirror/addon/edit/closebrackets.js'
          '<%= bowerLibs %>codemirror/addon/edit/matchbrackets.js'
          '<%= bowerLibs %>codemirror/keymap/vim.js'
        ]
        dest: '<%= buildDistDir %>libs.js'


    nodemon:
      server:
        script: 'server/server.js'
        options:
          # ignoredFiles: ['server-config-template.coffee']
          ext: 'js'
          watch: ['server']
          debug: true


    concurrent:
      target:
        tasks: ['nodemon', 'watch']
        options:
          logConcurrentOutput: true


    mochaTest:
      node:
        src: ['test/nodejs/**/*.coffee', 'test/shared/**/*.coffee']
        options:
          reporter: 'spec'
          ui: 'tdd'
          timeout: 5000
          require: 'test/nodejs-setup.js'


    karma:
      unit:
        configFile: 'karma.conf.coffee'


  # build/dist tasks
  grunt.registerTask 'build', ['coffee', 'copy:js', 'concat:js']
  grunt.registerTask 'distResources', ['less', 'copy:assetFonts', 'concat:css', 'concat:libs']
  grunt.registerTask 'distJs', ['copy:build', 'copy:dist']
  grunt.registerTask 'distDev', ['clean', 'build', 'distResources', 'distJs']

  # test tasks
  grunt.registerTask 'test', ['mochaTest', 'karma']

  # automated dev tasks
  grunt.registerTask 'client', ['distDev', 'watch']
  grunt.registerTask 'default', ['distDev', 'concurrent']
