# sourceTree format

cloudcode.testTree =
  "math":
    label: 'math'
    children:
      "noise":
        label: 'noise'
        children: {}
      "clamp":
        label: 'clamp'
        path: "math.clamp"
  "start":
    path: "..."



# loadedItems format

{
  "some.test.path":
    data: {} # data from the codeUnit model
    show:
      source: true
      test: false # ...
    editors:
      source:
        instance: 'cm_instance'
        domNode: 'domNode'
      test: {}
      docs: {}
      onUpdate: {}
}


# Test Database

cloudcode.testData =

  "math.clamp":
    source: "function(min, max, value) { \n\treturn Math.min(max, Math.max(min, value)); \n}"
    compiled: ""
    docs: ""
    test: ""
    path: "math.clamp"

  "math.lerp":
    source: "function(min, max, value) { return clamp(min, max, value) + 10; }"
    compiled: ""
    docs: ""
    test: ""
    path: "math.lerp"
    dependencies:
      "math.clamp": "clamp"

  "utils.clamp":
    source: "function(min, max, value) { return Math.min(max, Math.max(min, value)) }"
    compiled: ""
    docs: ""
    test: ""
    path: "utils.clamp"
