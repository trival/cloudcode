cloudcode.editor.nsTree =


  addNs: (tree, ns) ->

    path = ns.split '.'
    done = []

    intoBranch = (branch, label, childPaths) ->
      done.push label

      node = branch[label]
      unless node
        node =
          label: label
          path: done.join '.'
        branch[label] = node

      if childPaths.length
        node.children or= {}
        intoBranch node.children, childPaths.shift(), childPaths

      return

    intoBranch tree, path.shift(), path
    return


  fromNss: (nss) ->
    tree = {}
    for ns in nss
      cloudcode.editor.nsTree.addNs tree, ns
    tree
