cloudcode.editor.namespaces =


  exists: (ns) ->
    !!cloudcode.editor.state.namespaces[ns]


  addNew: (ns) ->
    {namespaces, state} = cloudcode.editor

    if namespaces.exists ns
      false
    else
      state.namespaces[ns] = true
      namespaces.updateTree()
      true


  remove: (ns) ->
    {codeItem, namespaces, state} = cloudcode.editor

    if namespaces.exists ns
      delete state.namespaces[ns]
      codeItem.remove ns
      namespaces.updateTree()
      true
    else
      false


  addMany: (nss) ->
    {namespaces, state} = cloudcode.editor

    for ns in nss
      state.namespaces[ns] = true

    namespaces.updateTree()
    return


  updateTree: ->
    {state, renderer, nsTree} = cloudcode.editor

    state.nsTree = nsTree.fromNss Object.keys state.namespaces
    renderer.renderTree()
    return


  createFromRemote: ->
    {dataClient, state, namespaces} = cloudcode.editor

    dataClient.getNamespaces()
      .then (nss) ->
        state.namespaces = {}
        namespaces.addMany nss
        return
      .catch (e) -> console.error e



