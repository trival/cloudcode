cloudcode.editor.constants =

  CM_OPTIONS_DEFAULT:
    mode: "javascript"
    theme: "monokai"
    smartIndent: true
    indentUnit: 4
    keyMap: "vim"
    lineNumbers: true
    viewportMargin: Infinity
    matchBrackets: true
    autoCloseBrackets: true

  CM_OPTIONS_COFFEESCRIPT:
    mode: "coffeescript"
    indentUnit: 2
